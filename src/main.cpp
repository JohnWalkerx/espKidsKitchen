#include <SoftwareSerial.h>
#include <Arduino.h>

#include "DFPlayerMiniWrapperAsync.h"

#include <Switch4Way.h>
#include <Hotplate.h>

static const int gc_pinDfPlayerSerialTx = D6;
static const int gc_pinDfPlayerSerialRx = D5;
static const int gc_pinHeatingSwitch1Bit0 = D2;
static const int gc_pinHeatingSwitch1Bit1 = D1;
static const int gc_pinHeatingSwitch2Bit0 = D4;
static const int gc_pinHeatingSwitch2Bit1 = D3;
static const int gc_pinHeating1NeoPixel = D7;
static const int gc_pinHeating2NeoPixel = D8;

static const int gc_hotplate1LedCount = 32;
static const int gc_hotplate2LedCount = 32;

static const unsigned long gc_hoteplateOffTimer = 240000;
static const int gc_playerVolume = 16;

Switch4Way g_switch1(gc_pinHeatingSwitch1Bit0, gc_pinHeatingSwitch1Bit1);
Switch4Way g_switch2(gc_pinHeatingSwitch2Bit0, gc_pinHeatingSwitch2Bit1);

Hotplate g_hotplate1(gc_hotplate1LedCount, gc_pinHeating1NeoPixel, gc_hoteplateOffTimer);
Hotplate g_hotplate2(gc_hotplate2LedCount, gc_pinHeating2NeoPixel, gc_hoteplateOffTimer);

SoftwareSerial g_mp3Serial(gc_pinDfPlayerSerialRx, gc_pinDfPlayerSerialTx);
DFPlayerMiniWrapper g_mp3Player;

void setup()
{
    Serial.begin(115200);

    // Init MP3 Module
    g_mp3Serial.begin(9600);
    delay(600);
    g_mp3Player.setDebugSerial(Serial);

    Serial.println("Initialize mp3-Module");
    g_mp3Player.begin(g_mp3Serial);
    delay(500);
    g_mp3Player.setVolume(gc_playerVolume);
    g_mp3Player.stop();

    g_switch1.setup();
    g_switch2.setup();

    g_hotplate1.setup();
    g_hotplate2.setup();
}

void loop()
{
    // Handle cyclic processing
    g_hotplate1.loop();
    g_hotplate2.loop();
    g_mp3Player.processCommunication();

    g_switch1.readSwitchState();
    g_switch2.readSwitchState();

    g_hotplate1.setLevel(g_switch1.switchValue());
    g_hotplate2.setLevel(g_switch2.switchValue());

    // Handle mp3
    static int lastInternalLevel1 {0};
    static int lastInternalLevel2 {0};
    static bool isPlaying {false};

    if (lastInternalLevel1 != g_hotplate1.currentInternalLevel() ||
        lastInternalLevel2 != g_hotplate2.currentInternalLevel())
    {
        Serial.println("Internal Level of Hotplate 1 changed to: " + String(g_hotplate1.currentInternalLevel()));
        Serial.println("Internal Level of Hotplate 2 changed to: " + String(g_hotplate2.currentInternalLevel()));

        lastInternalLevel1 = g_hotplate1.currentInternalLevel();
        lastInternalLevel2 = g_hotplate2.currentInternalLevel();

        if ((lastInternalLevel1 > 0 ||
             lastInternalLevel2 > 0) && !isPlaying)
        {
            Serial.println("Start playing");
            isPlaying = true;
            g_mp3Player.loop(1);
        }
        else if (lastInternalLevel1 == 0 &&
                 lastInternalLevel2 == 0)
        {
            Serial.println("Stop playing");
            isPlaying = false;
            g_mp3Player.stop();
        }
    }
}
