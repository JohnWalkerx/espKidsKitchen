#include "Switch4Way.h"

Switch4Way::Switch4Way(int pinBit0, int pinBit1, bool usePullUp)
{
    m_pinNumberBit0 = pinBit0;
    m_pinNumberBit1 = pinBit1;
    m_usePullUp = usePullUp;
}

void Switch4Way::setup()
{
    if (m_setupFinished)
    {
        return;
    }

    if (m_pinNumberBit0 == -1 || m_pinNumberBit1 == -1)
    {
        // TODO: Write error message
        return;
    }

    if (m_usePullUp)
    {
        pinMode(m_pinNumberBit0, INPUT_PULLUP);
        pinMode(m_pinNumberBit1, INPUT_PULLUP);
    }
    else
    {
        pinMode(m_pinNumberBit0, INPUT);
        pinMode(m_pinNumberBit1, INPUT);
    }

    m_setupFinished = true;
}

void Switch4Way::readSwitchState()
{
    if (m_usePullUp)
    {
        // Signal is low-active.
        m_stateBit0 = digitalRead(m_pinNumberBit0) > 0 ? false : true;
        m_stateBit1 = digitalRead(m_pinNumberBit1) > 0 ? false : true;
    }
    else
    {
        // Signal is high-active.
        m_stateBit0 = digitalRead(m_pinNumberBit0) > 0 ? true : false;
        m_stateBit1 = digitalRead(m_pinNumberBit1) > 0 ? true : false;
    }

    int switchValue {0};
    switchValue += m_stateBit0 ? 0b01 : 0b00;
    switchValue += m_stateBit1 ? 0b10 : 0b00;

    m_switchValue = static_cast<E_SwitchValue>(switchValue);
}

E_SwitchValue Switch4Way::switchValue() const
{
    return m_switchValue;
}
