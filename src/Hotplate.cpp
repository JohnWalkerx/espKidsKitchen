#include "Hotplate.h"

Hotplate::Hotplate(int ledCount, int pinNumber, unsigned long offTimer)
{
    m_ledCount = ledCount;
    m_pinNumber = pinNumber;
    m_offTimer = offTimer;

    m_lastChangeTimer = millis();

    m_ledFx = std::make_unique<WS2812FX>(m_ledCount, m_pinNumber, NEO_GRB + NEO_KHZ800);
}

void Hotplate::setup()
{
    if (!m_ledFx)
    {
        Serial.println("Error: No LED FX.");
        return;
    }

    m_ledFx->init();
}

void Hotplate::loop()
{
    if (!m_ledFx)
    {
        Serial.println("Error: No LED FX.");
        return;
    }

    checkTimer();
    m_ledFx->service();
}

void Hotplate::setLevel(int level)
{
    if (m_level == level)
    {
        return;
    }

    m_level = level;

    Serial.println("Hotplate with LED-Pin '" + String(m_pinNumber) + "' switched to level '" + String(level) + "'");

    resetTimer();

    switch (level)
    {
    case 0:
        showLevel0();
        break;
    case 1:
        showLevel1();
        break;
    case 2:
        showLevel2();
        break;
    case 3:
        showLevel3();
        break;
    default:
        showLevel0();
        break;
    }
}

int Hotplate::currentInternalLevel() const
{
    return m_internalLevel;
}

void Hotplate::showLevel0()
{
    m_internalLevel = 0;
    m_ledFx->stop();
}

void Hotplate::showLevel1()
{
    m_internalLevel = 1;
    m_ledFx->stop();
    m_ledFx->setBrightness(10);
    m_ledFx->setColor(RED);
    m_ledFx->setSpeed(3500);
    m_ledFx->setMode(FX_MODE_DUAL_SCAN);
    m_ledFx->start();
}

void Hotplate::showLevel2()
{
    m_internalLevel = 2;
    m_ledFx->stop();
    m_ledFx->setBrightness(40);
    m_ledFx->setColor(RED);
    m_ledFx->setSpeed(2000);
    m_ledFx->setMode(FX_MODE_RUNNING_LIGHTS);
    m_ledFx->start();
}

void Hotplate::showLevel3()
{
    m_internalLevel = 3;
    m_ledFx->stop();
    m_ledFx->setBrightness(100);
    m_ledFx->setColor(RED);
    m_ledFx->setSpeed(4000);
    m_ledFx->setMode(FX_MODE_BREATH);
    m_ledFx->start();
}

void Hotplate::checkTimer()
{
    auto timeDiff = millis() - m_lastChangeTimer;
    if (timeDiff > m_offTimer && !m_timerActive)
    {
        m_timerActive = true;
        showLevel0();
    }
}

void Hotplate::resetTimer()
{
    m_lastChangeTimer = millis();
    m_timerActive = false;
}
