# espKidsKitchen

[![status-badge](https://ci.codeberg.org/api/badges/JohnWalkerx/espKidsKitchen/status.svg)](https://ci.codeberg.org/JohnWalkerx/espKidsKitchen)

Electronic part of a self-build kitchen for kids.

An ESP8266 microcontroller is used to control everything.

It has two LED hotplate which plays a sound when they are on.
The hotplate have 3 different heating levels which are visualized by different animation patterns.
Each hotplate can be controlled separately.

It uses a 20.000 mAh PowerBank to supply all components with 5 V.

## Build

Clone the repository `git clone <remote-repo-url>`, change directory `cd espAudioAdventCalender` and initialize the submodules with `git submodule update --init`.

Install [PlatformIO](https://platformio.org/) extension in VSCode to compile and upload it to the ESP8266.

## Circuit diagram

You can find the circuit diagram [here](docs/eschema/espKidsKitchen.pdf).

## Used components

It uses following components:

- [ESP8266 Wemos D1 mini (clone)](https://www.berrybase.de/dev.-boards/esp8266-esp32-d1-mini/boards/d1-mini-esp8266-entwicklungsboard)
- [DFRobot DFPlayer mini](https://www.berrybase.de/sensoren-module/audio-schall/mp3-player-modul-mit-eingebautem-verst-228-rker)
- [DS1307 realtime clock](https://www.berrybase.de/neu/ds1307-realtime-clock/echtzeituhr-shield-f-252-r-d1-mini?c=2474)
- [Mono speaker](https://www.berrybase.de/bauelemente/elektromagnetische-bauelemente/lautsprecher/lautsprecher-flach-0-5w)
- [2x NeoPixel Ring with 32 LEDs](https://www.berrybase.de/sensoren-module/led/ws2812-13-neopixel/ringe/neopixel-ring-mit-32-ws2812-5050-rgb-leds)
- [3-way switch](https://de.elv.com/lorlin-drehschalter-3-stromkreise-4-stellungen-001124)
- [Knob for switch](https://de.elv.com/spannzangen-drehknopf-21-mm-durchmesser-fuer-6-mm-achse-000732), [Cap](https://de.elv.com/spannzangen-drehknopf-21-mm-durchmesser-fuer-6-mm-achse-000732) and [Arrow](https://de.elv.com/pfeilscheiben-grau-fuer-21-mm-spannzangen-drehknopf-000853)
- [Case](https://www.berrybase.de/bauelemente/gehaeuse/universalgehaeuse/halbschalengeh-228-use-124x50x72mm-schwarz)
- [USB C Outbreak Board](https://www.berrybase.de/sensoren-module/usb-seriell-lan/adafruit-usb-type-c-breakout-board-downstream-verbindung)
- [Perfboard](https://www.berrybase.de/raspberry-pi/raspberry-pi-computer/prototyping/lochrasterplatine-5x7cm-mit-einseitiger-kupferauflage)
- microSDHC card for DFPlayer mini

I used hot glue and some screws to mount everything.

## License

The source code is licensed under the [MIT Licence](LICENCE).
All pictures and documentation are licensed under the [Creative Commons Attribution-ShareAlike 4.0 International License.](https://creativecommons.org/licenses/by-sa/4.0/).

## Pictures

![Picture](docs/assets/001.jpg)
![Picture](docs/assets/002.jpg)
![Picture](docs/assets/003.jpg)
![Picture](docs/assets/004.jpg)
![Picture](docs/assets/005.jpg)
![Picture](docs/assets/006.jpg)
![Picture](docs/assets/007.jpg)
![Picture](docs/assets/008.jpg)
