#ifndef SWITCH4WAY_H
#define SWITCH4WAY_H

#include "Arduino.h"

enum E_SwitchValue
{
    EvSwitchValueOff = 0,
    EvSwitchValueOn1 = 1,
    EvSwitchValueOn2 = 2,
    EvSwitchValueOn3 = 3
};

class Switch4Way
{
public:
    Switch4Way(int pinBit0, int pinBit1, bool usePullUp = true);
    ~Switch4Way() = default;

    void setup();

    void readSwitchState();
    E_SwitchValue switchValue() const;

private:
    int m_pinNumberBit0 {-1};
    int m_pinNumberBit1 {-1};
    bool m_usePullUp {false};

    bool m_setupFinished {false};
    bool m_stateBit0 {false};
    bool m_stateBit1 {false};
    E_SwitchValue m_switchValue {EvSwitchValueOff};
};

#endif // SWITCH4WAY_H
