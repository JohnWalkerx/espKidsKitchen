#ifndef HOTPLATE_H
#define HOTPLATE_H

#include <memory>
#include <WS2812FX.h>

class Hotplate
{
public:
    Hotplate(int ledCount, int pinNumber, unsigned long offTimer = 0);
    ~Hotplate() = default;

    void setup();
    void loop();

    void setLevel(int level);
    int currentInternalLevel() const;

private:
    void showLevel0();
    void showLevel1();
    void showLevel2();
    void showLevel3();

    void checkTimer();
    void resetTimer();

    int m_ledCount {0};
    int m_pinNumber {-1};
    unsigned long m_offTimer {0};

    int m_level {-1};
    int m_internalLevel {0};
    unsigned long m_lastChangeTimer {0};
    bool m_timerActive {false};

    std::unique_ptr<WS2812FX> m_ledFx;
};

#endif // HOTPLATE_H
